//tests.js

var LayerAPI = require('layer-api');
// var prompt = require('prompt');
var Parse = require('parse/node');
Parse.initialize('BetDAREiiaig5uxPY68P', 'sampleJSKeyForLocal', 'sampleLocalKey');
Parse.serverURL = 'http://localhost:1337/parse'

// Initialize by providing your Layer credentials 
var layerClient = new LayerAPI({
    token: "K8xh9dQv7HwV9QJ5Md9g9B2MZ1ebIlgG5TrjIx0R9exNAmyF",
    appId: "layer:///apps/staging/c1176e86-e785-11e6-afcf-c4196c035e12"
});


var fns = {
    getConversation: function(request, response) {
        var cid = 'layer:///conversations/3ee33eee-92bc-4ac7-bbcb-4736f9d859b9';
        layerClient.conversations.get(cid, function(err, res) {
            if (err) return console.error(err);

            // conversation data
            var conversation = res.body;
            console.log(conversation);
            response.send(conversation);
        });
    },
    sendMessage: function(request, response) {
        var text = 'Sent from node.js';
        var sender = 'Hk8USlrEfL';
        var cid = 'layer:///conversations/3ee33eee-92bc-4ac7-bbcb-4736f9d859b9';
        layerClient.conversations.get(cid, function(err, res) {
            if (err) return console.error(err);

            // conversation data 
            var conversation = res.body;
            console.log('conversation:');
            console.log(conversation);

            layerClient.messages.sendTextFromUser(conversation.id, sender, text, function(err, res) {
                console.log(err || res.body);
                response.send(err || res.body);
            });
        });
    }
}



/*
// Get User Input
prompt.start();
prompt.get('userId', function(err, result) {
  if (err) done();
  else {
      var userId = result.userId;
      getAllConversations(userId);
  }
});



function getAllConversations(user){
  console.log('fetching convos for user ' + user);
  var params = {
    page_size: 50,
    sort_by: 'last_message' // `created_at` or `last_message`
  };
  
  layerClient.conversations.getAllFromUser('oZsSpjpLLu', params, function(err, res) {
    if (err) return console.error(err);

    var convos = res.body;
    console.log('convos for user: \n');
    console.log(convos);
  });
}

layerClient.conversations.get('layer:///conversations/7b770d4e-2ff3-484f-bec1-4b4c94ff48fe', function(err, res) {
  if (err) return console.error(err);

  // conversation data
  var conversation = res.body;
  console.log(conversation);
});
*/





/*
// Create a Conversation 
layer.conversations.create({participants: ['oZsSpjpLLu','Yn9pn52cuf']}, function(err, res) {
    var cid = res.body.id;
  
  console.log('convo created with id ', cid);
    // Send a Message 
    layer.messages.sendTextFromUser(cid, 'Yn9pn52cuf', 'Karsh2 to the OG Karsh', function(err, res) {
      console.log(err || res.body);
    });
});


var payload = {
  recipients: ['oZsSpjpLLu','Yn9pn52cuf'],
  sender: {
    name: 'The System'
  },
  parts: [
    {
      body: 'Hello, World!',
      mime_type: 'text/plain'
    }
  ]
};
layer.announcements.send(payload, function(err, res) {
  if (err) return console.error(err);
 
  // announcement data 
  var announcement = res.body;
});
*/


module.exports = function(request, response) {
    var fnName = request.query.fn;
    fns[fnName](request, response);
}