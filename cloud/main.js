// ****************     LAYER CHAT     *****************
// Layer Chat API authentication
var fs = require('fs');
var layer = require('./layer-module.js');
    // Should have the format of layer:///providers/<GUID>
    var layerProviderID = 'layer:///providers/c109d334-e785-11e6-afcf-c4196c035e12';
    // Should have the format of layer:///keys/<GUID>
    var layerKeyID = 'layer:///keys/84f80274-e912-11e6-9d19-a14c000000db';
    console.log('imported layer parse module');

var privateKey = fs.readFileSync('./cloud/layer-key.js').toString();
console.log('read layer key file');

layer.initialize(layerProviderID, layerKeyID, privateKey);
console.log('layer-parse module intialized.');

// Layer API
var LayerAPI = require('layer-api');
// Initialize by providing your Layer credentials
var layerClient = new LayerAPI({
	token: "K8xh9dQv7HwV9QJ5Md9g9B2MZ1ebIlgG5TrjIx0R9exNAmyF",
	appId: "layer:///apps/staging/c1176e86-e785-11e6-afcf-c4196c035e12"
});

console.log("connected to layer platform");

// Layer API
var LayerAPI = require('layer-api');


function removeLastName(name){
    return name.split(" ")[0]
}

Parse.Cloud.define("generateToken", function(request, response) {
  console.log("User from Request: " + request.User);
    var currentUser = request.user;
    if (!currentUser){
    	response.error('You need to be logged in!');
    	return;
    }
    var userID = currentUser.id;
    var nonce = request.params.nonce;
    if (!nonce){ response.error('Missing nonce parameter'); throw new Error('Missing nonce parameter');}

    response.success(layer.layerIdentityToken(userID, nonce));
});

/**	Check in a user into a place
	@params:
			+	user_location_lat	$GeoPoint
			+	user_location_lon	$GeoPoint
			+	place_id			$:String 	- Google Place ID
			+ 	place_name 			:String 	- Google Place name
			+  	place_location_lon	:GeoPoint 	- Location of the place.
			+  	place_location_lat	:GeoPoint 	- Location of the place.
			+ 	place_type			:[String]	- The types of this place.
			+ 	place_website		:String		- Website for this place.
			+ 	place_address		:String
*/

Parse.Cloud.define('check-in', function(request, response) {
    if (!request.user) {
        response.error('[check-in] error valid logged in users must make this request');
        return;
    }
    var paramsCheck = (    !request.params.user_location_lat ||
        !request.params.user_location_lon ||
        !request.params.place_id ||
        !request.params.place_location_lat ||
        !request.params.place_location_lon  );
    if (paramsCheck) {
        response.error('[check-in] error one or more params missing');
        return;
    }

    //Extract Data
    var user_location_lat = request.params.user_location_lat;
    var user_location_lon = request.params.user_location_lon;

    var place_id = request.params.place_id;

    var place_location_lat = request.params.place_location_lat;
    var place_location_lon = request.params.place_location_lon;

    if (request.params.place_country_code != null)
        var place_country_code = request.params.place_country_code;


    var dist = distance(user_location_lat, user_location_lon, place_location_lat, place_location_lon);
    var DISTANCE_LIMIT = process.env.DISTANCE_LIMIT || 30.0;

    console.log("[check-in] check into google place ", place_id);
    console.log("[check-in] distance: ", dist,
        "[check-in] LIMIT: ", DISTANCE_LIMIT);

    // Distance Filter
    if (dist > DISTANCE_LIMIT) {
        response.error('Distance exceeds ' + DISTANCE_LIMIT + ' meter limit for checking in');
        return;
    }

    // find place in db
    checkForPlaceInDB(place_id, function (result, error) {
        if (result != null) {
            // We found this place so check the user into it
            console.log("[check-in] checking in to existing place...");
            checkInUser(request.user, result, function (status, push_sent, error_string) {
                if (status == true && push_sent == true) {
                    console.log('[check-in] all done.')
                    //response.success('checked in to place '+result.id+'. push notif sent');
                    response.success('');
                }
                else {	// Status is false here - so something big failed - not push
                    console.log('[check-in] finshed with error' + error_string);
                    //response.error(error_string);
                    response.error('error checking in');
                }
            });
        }
        else {
            console.log("[check-in] registering new place on db...");

            // Place not found - A create place, B check user in
            var PlaceClass = Parse.Object.extend("Place");
            var newPlace = new PlaceClass();

            // Set Fields for new Place
            newPlace.set("name", request.params.place_name);
            newPlace.set("placeId", place_id);
            newPlace.set("address", request.params.place_address);

            if (place_country_code != null)
            newPlace.set("country_code", place_country_code);

            //newPlace.set("types", request.params.place_types);

            var point = new Parse.GeoPoint({latitude: place_location_lat, longitude: place_location_lon});
            newPlace.set("location", point);

            newPlace.save(null, {
                success: function (place) {
                    // New Place was saved
                    console.log('[check-in] new place registed on db with id ' + place.id);

                    checkInUser(request.user, place, function (status, push_sent, error_string) {
                        if (status == true && push_sent == true) {
                            var success_str = 'checked in to place ' + place.id + '. push notif sent';
                            console.log('[check-in] done. \t' + success_str);
                            //response.success(success_str);
                            response.success('');

                        } else { // Status is false here - so something big failed - not push

                            console.log('[check-in] error.', response);
                            //response.error(error_string);
                            response.error('error checking in');
                        }
                    });
                },
                error: function (place, error) {
                    // Return bad resp
                    var error_string = 'Failed to register this place on our DB.\nerror message: ' + error.message;
                    console.error('[check-in] ' + error_string);
                    response.error(error_string);
                }
            });
        }
    });

    /*
    Check the User In
    -NOTE: THis function Returns on callback @param
    @param: callback <function(status, push_sent, error_string)>
    */
    function checkInUser(user, place, callback) {
        // Find users in the same place
        var userQuery = new Parse.Query(Parse.User);
        userQuery.equalTo('checkedIn', place);


        // Find devices associated with these users
        var pushQuery = new Parse.Query(Parse.Installation);
        pushQuery.matchesQuery('user', userQuery);


        // DEBUG
        printQuery(userQuery);

        var name = user.get('name');

        var alertTitle = removeLastName(name) + " just checked in to this place!";

        // Send push notifications
        Parse.Push.send({
            where: pushQuery,
            data: {
                alert: alertTitle,
                badge: "Increment",
                sound: "cheering.caf",
                title: "A new person just arrived!",
                type: "user-check-in"
            }
        }, {
            useMasterKey: true
        }).then(function () {	// Push sent! - Time Stamp User

            console.log("[check-in] push notifications sent!");
            user.save({
                lastCheckInTime: new Date(),
                checkedIn: place
            }, {
                success: function (user) {
                    console.log('[check-in] saved user data!\n',
                        '[check-in] creating new user activity ..');

                    // Create new activity for user
                    createNewUserActivity(place, function (status, error) {
                        console.log('[check-in] user activity status: ' + status);
                        var err_str = null;
                        if (error) {
                            err_str = 'error creating new activity: ' + error.message;
                        }
                        callback(status, true, err_str);
                    });
                },
                error: function (userAgain, error) {
                    // Error while saving user
                    var error_str = 'failed to save user data.\nerror code: ' + error.message;
                    console.error('[check-in] ' + error_str);

                    callback(false, true, error_str);
                }, useMasterKey: true
            });

            // Push errors happen after this function
        }, function (error) {
            // There was a problem :(
            var error_str = "push notification error: " + error.message;
            console.error("[check-in] " + error_str);

            callback(false, false, error_str);
            return;
        });
    }

    //Haversine formula betwen two lat lon points
    function distance(lat1, lon1, lat2, lon2) {
        var p = 0.017453292519943295;    // Math.PI / 180
        var c = Math.cos;
        var a = 0.5 - c((lat2 - lat1) * p) / 2 +
            c(lat1 * p) * c(lat2 * p) *
            (1 - c((lon2 - lon1) * p)) / 2;

        return 12742 * Math.asin(Math.sqrt(a)) * 1000; // Answer in meters
    }

    // Find the place with same place id in database
    function checkForPlaceInDB(place_id, callback) {
        var PlaceClass = Parse.Object.extend("Place");
        var query = new Parse.Query(PlaceClass);
        query.equalTo("placeId", place_id);

        query.first({
            success: function (result) {
                callback(result, null);
            },
            error: function (error) {
                console.error("[check-in] Error: " + error.message);
                callback(null, error);
            }
        });
    }



    /// Create new user activity at a given place
    function createNewUserActivity(place, callback) {
        var UserActivity = Parse.Object.extend('UserActivity');
        var checkin = new UserActivity();
        var time = new Date();

        checkin.set('createdBy', request.user);
        checkin.set('place', place);
        checkin.set('type', 'checkin');		// Dare Activity
        checkin.set('when', time);

        checkin.addUnique('participantsIDs', request.user.id);
        checkin.addUnique('participants', request.user);

        checkin.set('readBy', []);

        console.log('[check-in] saving user activity...');
        checkin.save(null, {
            success: function (a) {
                callback(true, null);
            },
            error: function (a, error) {
                callback(false, error);
            }
        });
    }


});

/**	Checks out a user out of a place
	@params:
			+	request.user	$PFUser
*/
Parse.Cloud.define('check-out', function(request, response){
	if (!request.user){
		response.error('[check-out] request must pass in user in params');
	}

	//Extract Data - current User is in request.user
	var place = request.user.get('checkedIn');
	request.user.unset('lastCheckInTime');
	request.user.unset('checkedIn');

	request.user.save(null, {
		success: function(user) {

            response.success('')
		},
		error: function(userAgain, error) {
			// The save failed.
			response.error(error.message);
		}, useMasterKey:true
	});




});



/**	Send a Dare to User
	@params:
			+	request.user(from)	$ objectId:String
			+	to					$ objectId:String
*/
Parse.Cloud.define('dare', function(request, response){
	var place = request.user.get('checkedIn');

	// Asserts
	if (!request.user){
		response.error('user must be logged in to make a request');
		return;
	}
	else if (!request.params.to){
		response.error("missing @param 'to'");
		return;
	}
	else if (!place){
		response.error('You must be checked in to Dape');
		return;
	}
	else if (!request.params.place){
		response.error("missing @param 'place'. this is where the to user is");
		return;
	}
	else if (request.params.place != place.id){
		response.error('The current user is not checked in ');
		return;
	}

	// Create New User Actvity - aka Dare
	var DareActivity = Parse.Object.extend('UserActivity');
	var UserClass = Parse.Object.extend('User');
	var dare = new DareActivity();
	var time = new Date();
	var toPointer = UserClass.createWithoutData(request.params.to);
	var to = request.params.to;

            dare.set('createdBy', request.user);
            dare.set('place', place);
            dare.set('type', 'dare');		// Dare Activity
            dare.set('when', time);

            dare.addUnique('participantsIDs', to);			// Add only ObjectID<String> here not whole ptr
            dare.addUnique('participantsIDs', request.user.id);

            dare.addUnique('participants', toPointer);		// Add only <PFUser>
            dare.addUnique('participants', request.user);
            dare.set('readBy', []);

            dare.save(null, {
                success: function(dare) {
                    // Send out Push Notifications
                    var currentUserName = request.user.get('name');
                    sendOutPushNotifications(removeLastName(currentUserName), to, function(status, error_message){

                        // The save was successful.
                        if (status == true)
                            response.success('dape sent'); // push sent to' + to);
                        else
                            response.success('dape sent'); // Push error:' + error_message);
                    });
                },
                error: function(dare, error) {
                    // The save failed.  Error is an instance of Parse.Error.
                    response.error('cannot send dape. ' + error.message);
                }
            });

    })




	/** fromName - Name of the user sending the dare
		targetUser - $String - Object Id of 'TO' User
	*/
	function sendOutPushNotifications(name, targetUser, callback) {
        // Find users in the same place
        var userQuery = new Parse.Query(Parse.User);
        userQuery.equalTo('objectId', targetUser);
        // Find devices associated with these users
        var pushQuery = new Parse.Query(Parse.Installation);
        pushQuery.matchesQuery('user', userQuery);

        // Send push notifications
        Parse.Push.send({
            where: pushQuery,
            data: {
                alert: "Dape received! Check it out!",
                badge: "Increment",
                sound: "cheering.caf",
                title: "New Dape",
                type: "user-dare"
            }
        }, {
            useMasterKey: true
        }).then(function () {
            // Push sent!
            console.log("[dare] push notifications sent!");
            callback(true, null);

        }, function (error) {
            // There was a problem :(
            var error_str = "push notification error: " + error.message;
            console.error("[dare] " + error_str);
            callback(false, error_str);
        });
    }



function checkIfUserIsCheckedInToSamePlace(userID, place, callback){
    var UserClass = Parse.Object.extend("User");
    var query = new Parse.Query(UserClass);

    query.equalTo("id", userID)


    query.first({
        success: function (result) {
            if (result != null) {
                if (result.get("checkedIn") == place) {
                    callback(true, null);
                } else {
                    callback(false, null);
                }
            } else {
                callback(false, null);
            }

        },
        error: function (error) {
            console.error("Validate User is Checked In Error: " + error.message);
            callback(false, error);
        }
    });
}

/*
Parse.Cloud.define('create-convo', function(request, response){
	var metadata = {
		title: 'conversation between ' + from.id + ' and ' + to
	};
	console.log('creating conversation between ' + from.id + ' & ' + to);
	layerClient.conversations.create({participants: [from.id, to], metadata }, function(err, res) {
	  	var cid = res.body.id;


	 	if (err){
			response.error(err.message);
			return;
	 	}

		console.log('convo created with id ', cid);

	  	// Send a Message
	  	layerClient.messages.sendTextFromUser(cid, from, text, function(err, res) {
	    	console.log(err || res.body);
	    	if (err){
	    		response.error(err.message);
	    	}
	    	else{
	    		response.success('conversation created with id ' + cid);
	    	}
	  	});
	 });
});
*/

/** Registers Conversation Between Users on Parse
	@params :
		+	request.user (from)	$ objectId:String <User>
		+	to					$ objectId:String <User>
		+ 	conversationID		$ LYRConversationIdentifier:String
*/
Parse.Cloud.define('register-chat', function(request, response){
	// Create a Conversation
	var from = request.user;
	var to = request.params.to;					// << Warning this in object id not Parse.User
	var to_place = request.params.to_place;		// << Warning this in object id to PFObject.ObjectId
	var convo_id = request.params.conversationID;
	var place = request.user.get('checkedIn');

	// Assert Checks
	if (!from || !to){
		response.error("@params 'from' or 'to' missing");
		return;
	}
	else if (!convo_id){
		response.error("@param 'Layer conversation identifier' missing");
		return
	}
	else if (!place){
		response.error('user must be checked in to start convo to another user');
		return;
	}
	else if (!to_place){
		response.error("'to_place' must be passed in @param");
		return;
	}
	else if (to_place != place.id){
		response.error("assert: users must be at same place to chat with one another");
		return;
	}

	// Create New User Actvity - aka Conversation on Parse
	var UserActivity = Parse.Object.extend('UserActivity');
	var UserClass = Parse.Object.extend('User');
	var convo = new UserActivity();
	var time = new Date();
	var toPointer = UserClass.createWithoutData(to);

	convo.set('createdBy', request.user);
	convo.set('place', place);
	convo.set('type', 'conversation');				// Convo Activity
	convo.set('when', time);
	convo.set('cid', convo_id);

	convo.addUnique('participantsIDs', to);			// Add only ObjectID<String> here not whole ptr
	convo.addUnique('participantsIDs', from.id);

	convo.addUnique('participants', toPointer);		// Add only <PFUser>
	convo.addUnique('participants', from);
	convo.set('readBy', []);

	convo.save(null, {
		success: function(convo) {
			console.log('user activity created with id ' + convo.id);
			// Update conversation Meta Data
			var metadata = {
				title: 'conversation between ' + from.id + ' & ' + to,
				UserActivityId: convo.id,
				PlaceId: place.id
			};

			layerClient.conversations.setMetadataProperties(convo_id, metadata, function(err, res) {
				if (err){
					console.error(err);
					response.success('convo registered! Id: ' + convo.id + ' BUT metadata NOT set.');
				}
				else{
					response.success('convo registered! Id: ' + convo.id + ' & metadata set.');
				}
			});
		},
		error: function(convo, error) {

			response.error('error registering convo: ' + error.message);
		}
	});
});


/*			Before Save Triggers*/
Parse.Cloud.beforeSave('UserActivity', function(request, response) {
  if (!request.object.get('participants')) {
    response.error("you cannot have null participants");
  }
  else if (request.object.get('participants').length > 2) {
    response.error("you must have 1-2 participants in each activity");
  }
  else if (!request.object.get('place')) {
    response.error("you must be in a place to create new UserActivity");
  } else {
    response.success();
  }
});


// Helpers:
function printQuery(query){
	query.find({
	  success: function(results) {
	    console.log("[DEBUG] found " + results.length + " objects:");
	    // Do something with the returned Parse.Object values
	    for (var i = 0; i < results.length; i++) {
	      var object = results[i];
	      console.log('\t\t* ' + object.id + ' - ' + object.get('name'));
	    }
	  },
	  error: function(error) {
	    console.error("[DEBUG] error: " + error.code + " " + error.message);
	  }
	});
}
