// Example express application adding the parse-server module to expose Parse
// compatible API routes.

var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');
const chalk = require('chalk');
const assert = require('assert');
var bodyParser = require('body-parser');
var test = require('./cloud/tests');
const controller = require('./controllers/index');

//Variables Not displaying from env filename
var APP_ID="BetDAREiiaig5uxPY68P"
var BUNDLE_ID="com.epoke.dare"
var CLIENT_KEY="HPE2ylbFh&HFAHgfiH@nAU"
var DISTANCE_LIMIT=150
var JS_KEY="KeUHDRcf4Ytfrj+kC4Ea}d3"
var MASTER_KEY="REEUfRx0pB98ciTTQamPQWRKc"
var MONGODB_URI="mongodb://heroku_h9sw2hps:t4ul3rk1ana5jpdb4m5o5o1rpl@ds119578.mlab.com:19578/heroku_h9sw2hps"
var PARSE_MOUNT="/parse"
var REST_KEY="DvNgdtV6WJVmQhfGMwmRBEh"
var SERVER_URL="https://dare-server.herokuapp.com/parse"

const testingMode = process.env.TESTING_MODE || false;
if (testingMode){
    // Import keys here
    console.error(  chalk.red("⚠️  Warning: Testing mode",
                    "specify your ENV in ./keys/keys.env file "));
    const env = require('env2')('./keys/keys.env');
}

var databaseUri = process.env.DATABASE_URI || MONGODB_URI;
var bundleId = BUNDLE_ID;

if (!databaseUri)
  console.log(chalk.red('Error: Database URI not specified!'));


assert(databaseUri, 'Cannot launch server without DATABASE_URI');
assert(APP_ID, 'APP_ID missing');
assert(CLIENT_KEY, 'CLIENT_KEY missing');
assert(JS_KEY, 'JS_Key missing');
assert(MASTER_KEY, 'MASTER_KEY missing');
assert(REST_KEY, 'REST_KEY missing');
assert(bundleId, 'Bundle identifier missing');

// Create Parse Server
var api = new ParseServer({
  databaseURI: databaseUri,
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js',

  appId: APP_ID,
  clientKey: CLIENT_KEY,
  javascriptKey: JS_KEY,
  masterKey: MASTER_KEY, // Add your master key here. Keep it secret!
  restAPIKey: REST_KEY,

  facebookAppIds: ['1171216479580040'],            // Dare App Facebook ID

  push: {
    android: {
      senderId: '698347013266', // The Sender ID of GCM
      apiKey: 'AIzaSyCPa3CwYvYpvdtqg74KikHJsRbWvANHAro' // The Server API Key of GCM
    },
    ios:[
      // { // Legacy certificate
      //   pfx: 'certs/dare_development.p12', // The filename of private key and certificate in PFX or PKCS12 format from disk
      //   bundleId: 'com.Dare.ios', // The bundle identifier associate with your app
      //   production: false // Specifies which environment to connect to: Production (if true) or Sandbox
      // },
      { // Company Dev
        pfx: 'certs/epoke_apn_development.p12',
        bundleId: bundleId,
        production: false
      },
      { // Company Production
        pfx: 'certs/epoke_apn.p12',
        bundleId: bundleId,
        production: true
      }
    ]
  },
  serverURL: SERVER_URL || 'http://localhost:1337/parse',  // Don't forget to change to https if needed
  liveQuery: {
    classNames: [] // List of classes to support for query subscriptions
  }
});
// Client-keys like the javascript key or the .NET key are not necessary with parse-server
// If you wish you require them, you can set them as options in the initialization above:
// javascriptKey, restAPIKey, dotNetKey, clientKey

var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

// Parse Dashboard
var ParseDashboard = require('parse-dashboard');
var allowInsecureHTTP = false
var dashboard = new ParseDashboard({
  "apps": [
    {
      "serverURL": SERVER_URL || 'http://localhost:1337/parse',
      "appId": APP_ID,
      "masterKey": MASTER_KEY,
      "appName": "Dape"
    }
  ],
  "trustProxy": 1,
  "users": [
    {
      "user":"mathias",
      "pass":"dape2017"
    },
    {
      "user":"karsh",
      "pass":"dape2017"
    }
  ]
}, allowInsecureHTTP);

// make the Parse Dashboard available at /dashboard
app.use('/dashboard', dashboard);

// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
var mountPath = PARSE_MOUNT || '/parse';
app.use(mountPath, api);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function(req, res) {
  res.status(200).send('I am Dare app.');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
app.get('/test', test);
//handle layer messages
app
  .route('/webhooks/layer-message-created')
  .get(controller.handleLayerMessage)
  .post(controller.handleLayerMessage);
//near by places search passing the lat long
app.post('/findNearbyPlaces', controller.findNearbyPlaces);
var port = process.env.PORT || 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('dare-server running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
