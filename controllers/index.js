var CryptoJS = require("crypto-js");
const LAYER_SIGNATURE_KEY = 'dape-app-secret';
const GOOGLE_PLACES_API_KEY = 'AIzaSyAwfn32w1iVMfqvMvgemtkstgTVJYAGGhY';
const GOOGLE_PLACES_OUTPUT_FORMAT = "json";
const SUPPORTED_PLACES_TYPES = ["art_gallery", "bar", "cafe",
    "casino", "church", "gym",
    "library", "mosque", "museum",
    "night_club", "restaurant", "school",
    "synagogue", "university"
]
var GooglePlacesApi = require('googleplaces');
module.exports = {
    handleLayerMessage(request, response) {
        const params = Object.assign(request.query, request.body);
        console.log('called webhook');
        console.log('query and body ', request.query, request.body, request);
        console.log('params =', params);
        console.log('headers:', request.headers);
        if (!params.verification_challenge) {
            try {
            const signature = CryptoJS.HmacSHA1(JSON.stringify(params), LAYER_SIGNATURE_KEY).toString();
            const webHookSignature = request.header('layer-webhook-signature');
            console.log('secretHash =', signature);
            console.log('layer webhook signature=', webHookSignature);

            var MessageClass = Parse.Object.extend("Message");
            var newMessage = new MessageClass();
            newMessage.set('updatedAt', new Date());
            newMessage.set('event', params.event);
            newMessage.set('actor', params.actor);
            newMessage.set('conversation', params.message.conversation);
            newMessage.set('sent_at', params.message.sent_at);
            newMessage.set('sender', params.message.sender);
            newMessage.set('message', params.message);
            newMessage.save(null, {
                success: function(message) {
                    console.log('save success =', message);
                },
                error: function(message, error) {
                    console.log('error =', error.message);
                }
            })
            if (signature === webHookSignature) {
                console.log('signature matched')
            } else {
                console.log('SIGNATURE mismatch. Please verify secret key is correct on layer config');
            }
        } catch (e){
            console.log('error', e)
        }
        }
        response.status(200).send(params.verification_challenge);
    },
    findNearbyPlaces(request, response) {
    	const lat = request.body.lat || 40.7127;
    	const long = request.body.long || -74.0059;
        const nearBySearch = new GooglePlacesApi(GOOGLE_PLACES_API_KEY, GOOGLE_PLACES_OUTPUT_FORMAT).nearBySearch;
        var parameters = {
            location: [lat, long],
            type: SUPPORTED_PLACES_TYPES.join('|'),
            rankby: 'distance'
        };
        nearBySearch(parameters, function(error, googleResp) {
            if (error) throw error;
            response.send(googleResp.results);

        });
    }
}